import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Livros } from './models/livros';


@Injectable({
  providedIn: 'root'
})
export class LivrosService {

  url = 'http://localhost:3333/livros'; // api rest

  // injetando o HttpClient
  constructor(private httpClient: HttpClient) { }

  // Headers
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  // Obtem todos os livros
  getLivros(): Observable<Livros[]> {
    return this.httpClient.get<Livros[]>(this.url)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  // Obtem um livro pelo id
  getLivroById(id: number): Observable<Livros> {
    return this.httpClient.get<Livros>(this.url + '/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // salva um livro
  saveLivro(livro: Livros): Observable<Livros> {
    return this.httpClient.post<Livros>(this.url, JSON.stringify(livro), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // utualiza um livro
  updateLivro(livro: Livros): Observable<Livros> {
    return this.httpClient.put<Livros>(this.url + '/' + livro.id, JSON.stringify(livro), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // deleta um livro
  deleteLivro(livro: Livros) {
    return this.httpClient.delete<Livros>(this.url + '/' + livro.id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };

}
