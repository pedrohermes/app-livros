import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { from } from 'rxjs';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import {LayoutComponent} from './layout/layout.component';
import { LivrosComponent } from './livros/livros.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'livros', component: LivrosComponent},
  {path: 'home', component: HomeComponent},
  {path: '', component: LayoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
