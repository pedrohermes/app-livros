import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UserFormComponent } from './user-form/user-form.component';
//import {FormsModule} from '@Angular/forms'





@NgModule({
  declarations: [UserFormComponent],
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    
  ],
  exports: [
    UserFormComponent
  ]
})

export class UsuariosModule { }
