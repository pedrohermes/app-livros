//Importação dos módulos principais do angular
//Importação do módulo LivrosService que vai fazer a conexão com a api 
//O módulo LivroService é quem vai processar os retornos da api para que
//o component envie os dados para o arquivo html
import { Component, OnInit } from '@angular/core';
import { LivrosService} from '../livros.service'
import { Livros } from '../models/livros';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-livros',
  templateUrl: './livros.component.html',
  styleUrls: ['./livros.component.css']
})
export class LivrosComponent implements OnInit {

  livro = {} as Livros;
  livros!: Livros[];
  
  //O construtor inicializa com o módulo de serviços
  constructor(private livroService: LivrosService) {}
    
  ngOnInit() {
     this.getLivros();
  }

  // defini se um livro será criado ou atualizado
  saveLivro(form: NgForm) {
    if (this.livro.id !== undefined) {
      this.livroService.updateLivro(this.livro).subscribe(() => {
        this.cleanForm(form);
      });
    } else {
      this.livroService.saveLivro(this.livro).subscribe(() => {
        this.cleanForm(form);
      });
    }
  }

  // Chama o serviço para obtém todos os livros
  getLivros() {
    this.livroService.getLivros().subscribe((livors: Livros[]) => {
      this.livros = this.livros;
      return this.livros;
    });
  }

  // deleta um livro
  deleteLivro(livro: Livros) {
    this.livroService.deleteLivro(livro).subscribe(() => {
      this.getLivros();
    });
  }

  // copia o livro para ser editado.
  editLivro(livro: Livros) {
    this.livro = { ...livro };
  }

  // limpa o formulario
  cleanForm(form: NgForm) {
    this.getLivros();
    form.resetForm();
    this.livro = {} as Livros;
  }

}
