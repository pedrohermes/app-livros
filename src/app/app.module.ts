import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//import { from } from 'rxjs';
import { FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsuariosModule } from './usuarios/usuarios.module';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import {UsuariosService} from './usuarios.service';
import { LayoutComponent } from './layout/layout.component';
import { HttpClientModule } from '@angular/common/http';
import { LivrosComponent } from './livros/livros.component';





@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    LayoutComponent,
    LivrosComponent,
    
  
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UsuariosModule,
    FormsModule,
    HttpClientModule
  ],

  providers: [UsuariosService],
  bootstrap: [AppComponent]
})
export class AppModule { }




