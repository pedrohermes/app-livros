import { Component } from '@angular/core';
import { from } from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

    user: string = 'pedro@teste.com';
    senha: string = '12345';
    login: boolean = false;
    cadastro: boolean = false;

  constructor(private router: Router) { 
    
  }

  onSubmit(){
    this.router.navigate(['/home'])
  }

  cadastrar(event: { stopPropagation: () => void; preventDefault: () => void; }){
    event.stopPropagation();
    event.preventDefault();
    this.cadastro = true;
  };
  cancelarCadastro(){
    this.cadastro = false;
  }

}
